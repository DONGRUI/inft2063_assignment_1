# **Cash Register User Manual**
=============================
## 1. Welcome and transaction:
System will display welcome section "Welcome to DONGRUI Supermarket!" and system will ask user to enter cash register balance. After that System will display 
"Do you want to make a new transaction? (y|n) ". User has two option: "y" or "n".( if user input others, the system will display "You have typed wrong, Please reEnter.")
User enter "y": continue to buy item, User enter "n": quit.

## 2. Buy item
System will display "Please enter the item's name: ", "Please enter thee item's cost: " to ask user to enter item's name and cost. (If User has enter a same item's name, System 
will display "You have already add this item!"). there is a tip "Is all items have been bought? (y|n) " when user entered item's cost. User has two option: "y" or "n".
(if user input others, the system will display "You have typed wrong, Please reEnter."). User enter "y": return to checkout, User enter "n": Keep buying. 

## 3. Checkout
System will display "Do you have $20 voucher that needs to apply?(y/n) " to ask whether user wants to discount. If user enter "y", system will display "Please provide your voucher number:"
after that system will display the item's name list and total cost("You selected()items, the amount you need to pay is:" ), then system will ask user "Please enter the cash amount tendered: "
and calculate the change("Amount of change required:"). Lastly, system has a function which print receipt("Do you want to print a receipt? (enter y to get receipt) ")
and display receipt which include the item's name, price, checkout current date and time, total cost, user paid, change.

## 4. Balance of cash register
System will display "Balance of the Cash Register". 

   
   
   
   
   