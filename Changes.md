# **Tools for software development - Assignment 1 - Changes**

--------------------------------------------------------------

## **_1. Milestone 1: Fixing the Bugs:_**
         - Fix spelling errors
         - Change value errors
         - Display balance
         - Add dollar signs before cash value
         
## **_2. Milestone 2: Adding Features:_**
         - Add a while loop after enter float to ask wish to exit
         - Remove balance after each transation
         - Add while loop to allow multiple items and calculate change    
         - Add a welcome message 
         - Create a new branch for Chinese version of software
         - Add error handling when user unexpected input
         - Print a receipt after transaction
         
## **_3. Milestone 3: New feature:_**
         - Display current date and time when print receipt
         - Add voucher function to discount the cost




