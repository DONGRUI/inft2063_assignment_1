package tools;

import java.util.ArrayList;
import java.util.Scanner;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class CashRegister_ChineseVersion
{
	public static void main(String[] args)
	{
		String input;
		String choice;
		String item_name;
		double balance;
		double cost;
		double cash = 0;
		double change;
		double needToPay = 0;
		double totalCost = 0;
		boolean quit = false;
		int voucher = 20;
		// dataformat to show the transaction time
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        //Arraylist to store items which customer choose, this array as "Shopping basket"
		ArrayList<Transaction> transcations = new ArrayList<>();
		Scanner scanner = new Scanner(System.in);
		System.out.println("欢迎来到栋锐超市！");
		System.out.print("请输入收银机余额： ");
		input = scanner.nextLine();
		balance = Double.parseDouble(input);
        // Ask customer if want to make transaction
		while(!quit){
		    System.out.print("您要进行新交易吗？ （是 | 否） ");
		    choice = scanner.nextLine();
		    //input check
	    	while(!choice.equalsIgnoreCase("是") && !choice.equalsIgnoreCase("否")) {
			    System.out.println("您输入有误，请重新输入"); 
			    System.out.print("您要进行新交易吗？ （是 | 否）");
			    choice = scanner.nextLine();
		    }
		    cash = 0;
			needToPay = 0;
			totalCost = 0;
			transcations = new ArrayList<Transaction>();
			// make a new transaction 
			if(choice.equalsIgnoreCase("是")){
				boolean allGoods = false;
				boolean discount = false;
				//check if the customer finish selecting item
				while (!allGoods){
					System.out.print("请输入商品名称：");
					input = scanner.nextLine();
					item_name = input;

					System.out.print("请输入您的商品价格：");
					input = scanner.nextLine();
					cost = Double.parseDouble(input);

					Transaction trans = new Transaction(item_name, cost);
					//check if the customer has already add the item
					boolean exist = false;
					for (Transaction t : transcations){
						if (t.getName().equals(trans.getName())){
							if (t.getCost() == trans.getCost()){
								exist = true;
							}
						}
					}
					if (!exist){
						transcations.add(trans);
						needToPay += trans.getCost();
						totalCost += trans.getCost();
					}
					else{
						System.out.println("您已经添加了此商品！");
					}
					//ask if customer want to buy other items
					System.out.print("是否所有物品都已购买？ （是 | 否） ");
					choice = scanner.nextLine();
					while(!choice.equalsIgnoreCase("是") && !choice.equalsIgnoreCase("否")) {
						System.out.println("您输入有误，请重新输入"); 
						System.out.print("是否已输入所有商品？ （是 | 否）");
						choice = scanner.nextLine();
					}

					if (choice.equalsIgnoreCase("是") ){
						allGoods = true;
					}
					else if (choice.equalsIgnoreCase("否")){
						System.out.println("继续购物!");
					}
					else{
						System.out.println("错误的指示！ 请检查");
					}
				}

				boolean enough = false;
				// check if the customer has paid enough money
			while (!enough){
				    //show how much customer need to pay
				    //ask whether has voucher available on this transactions.
				    
				    if (!discount) {
						System.out.print("您是否有需要使用$ 20优惠券？（是 / 否）");
						choice = scanner.nextLine();
						while (!choice.equalsIgnoreCase("是") && !choice.equalsIgnoreCase("否")) {
							// input type check
							System.out.println("您输入有误，请重新输入。");
							System.out.print("您是否有需要使用$ 20优惠券？（是 / 否）");
							choice = scanner.nextLine();
						}
						discount = true;
						if (choice.equals("是")) {
							// if customer wants use voucher, ask for input of voucher, 
							// and verify it,deduct the amount of needToPay.
							System.out.print("请提供您的优惠券编号：");
							String voucherNum = scanner.nextLine();
							System.out.println("你选择了 " + transcations.size()
									+ " 商品, 您需要支付的金额为： " + (needToPay - voucher));
							needToPay -= voucher;
							totalCost -= voucher;
						} else {
							System.out.println("你选择了 " + transcations.size()
									+ " 商品, 您需要支付的金额为: " + needToPay);
						}
					}
					else{
						System.out.println("你选择了 " + transcations.size() + "  商品, 您需要支付的金额为: "
								+ needToPay);
					}
					
					int paid = 0;
					System.out.print("请输入支付的现金金额：");
					input = scanner.nextLine();
					paid += Double.parseDouble(input);
					cash += paid;

					needToPay -= paid;
					// paid enough
					if (needToPay <= 0) {
						enough = true;
					} else {
						System.out.println("哎呀，还不够。");
					}
				}
				balance += cash;
				change = cash - totalCost;
				System.out.println("所需要找零的金额: " + change);

				balance -= change;
				System.out.print("您要打印收据吗？ （输入y获取收据）");
				choice = scanner.nextLine();
				while(!choice.equalsIgnoreCase("y") && !choice.equalsIgnoreCase("n")) {
					System.out.println("您输入的错误，请重新输入。"); 
					System.out.print("您要打印收据吗？ （输入y获取收据）");
					choice = scanner.nextLine();
				}
				if (choice.equalsIgnoreCase("y")){
					System.out.println("***********收据***********");
					int num = 1;
					//show all the item in the basket
					for (Transaction t: transcations){
						System.out.println( num + ". 姓名: *" + t.getName() + "*  价格: *" + t.getCost() + "*"
						+ "    Date: *" + dtf.format(t.getDate())+ "*");
						num ++;
					}
					//payment info
					System.out.println("\n总消费: " + totalCost);
					System.out.println("你支付了: " + cash);
					System.out.println("找零: " + change);
					System.out.println("******祝你有美好的一天!******");
					transcations.clear();
				}
				else{
					System.out.println("******祝你有美好的一天!******");
				}
			}
			// quit the program
			else if (choice.toLowerCase().equals("n")){
				quit = true;
			}
		}
		System.out.println("\n收银机余额: " + balance);
	}
}
