package tools;

import java.util.ArrayList;
import java.util.Scanner;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class CashRegister
{
	public static void main(String[] args)
	{
		String input;
		String choice;
		String item_name;
		double balance;
		double cost;
		double cash = 0;
		double change;
		double needToPay = 0;
		double totalCost = 0;
		boolean quit = false;
		int voucher = 20;
		// dataformat to show the transaction time
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        //Arraylist to store items which customer choose, this array as "Shopping basket"
		ArrayList<Transaction> transcations = new ArrayList<>();
		Scanner scanner = new Scanner(System.in);
		System.out.println("Welcome to DONGRUI Supermarket!");
		System.out.print("Please enter cash register's balance: ");
		input = scanner.nextLine();
		balance = Double.parseDouble(input);
        // Ask customer if want to make transaction
		while(!quit){
		    System.out.print("Do you want to make a new transaction? (y|n) ");
		    choice = scanner.nextLine();
		    //input check
	    	while(!choice.equalsIgnoreCase("y") && !choice.equalsIgnoreCase("n")) {
			    System.out.println("You have typed wrong, Please reEnter."); 
			    System.out.print("Do you want to make a new transaction? (y|n) ");
			    choice = scanner.nextLine();
		    }
		    cash = 0;
			needToPay = 0;
			totalCost = 0;
			transcations = new ArrayList<Transaction>();
			// make a new transaction 
			if(choice.equalsIgnoreCase("y")){
				boolean allGoods = false;
				boolean discount = false;
				//check if the customer finish selecting item
				while (!allGoods){
					System.out.print("Please enter the item's name: ");
					input = scanner.nextLine();
					item_name = input;

					System.out.print("Please enter thee item's cost: ");
					input = scanner.nextLine();
					cost = Double.parseDouble(input);

					Transaction trans = new Transaction(item_name, cost);
					//check if the customer has already add the item
					boolean exist = false;
					for (Transaction t : transcations){
						if (t.getName().equals(trans.getName())){
							if (t.getCost() == trans.getCost()){
								exist = true;
							}
						}
					}
					if (!exist){
						transcations.add(trans);
						needToPay += trans.getCost();
						totalCost += trans.getCost();
					}
					else{
						System.out.println("You have already add this item!");
					}
					//ask if customer want to buy other items
					System.out.print("Is all items have been bought? (y|n) ");
					choice = scanner.nextLine();
					while(!choice.equalsIgnoreCase("y") && !choice.equalsIgnoreCase("n")) {
						System.out.println("You have typed wrong, Please reEnter."); 
						System.out.print("Is all items have been entered? (y|n) ");
						choice = scanner.nextLine();
					}

					if (choice.equalsIgnoreCase("y") ){
						allGoods = true;
					}
					else if (choice.equalsIgnoreCase("n")){
						System.out.println("Keep buying!");
					}
					else{
						System.out.println("Wrong instruction! Please check. ");
					}
				}

				boolean enough = false;
				// check if the customer has paid enough money
			while (!enough){
				    //show how much customer need to pay
				    //ask whether has voucher available on this transactions.
				    
				    if (!discount) {
						System.out.print("Do you have $20 voucher that needs to apply?(y/n) ");
						choice = scanner.nextLine();
						while (!choice.equalsIgnoreCase("y") && !choice.equalsIgnoreCase("n")) {
							// input type check
							System.out.println("You have typed wrong, Please reEnter.");
							System.out.print("Do you have $20 voucher that needs to apply?(y/n) ");
							choice = scanner.nextLine();
						}
						discount = true;
						if (choice.equals("y")) {
							// if customer wants use voucher, ask for input of voucher, 
							// and verify it,deduct the amount of needToPay.
							System.out.print("Please provide your voucher number:");
							String voucherNum = scanner.nextLine();
							System.out.println("You selected " + transcations.size()
									+ " items, the amount you need to pay is: " + (needToPay - voucher));
							needToPay -= voucher;
							totalCost -= voucher;
						} else {
							System.out.println("You selected " + transcations.size()
									+ " items, the amount which you need to pay is: " + needToPay);
						}
					}
					else{
						System.out.println("You selected " + transcations.size() + " items, the amount which you need to pay is: "
								+ needToPay);
					}
					
					int paid = 0;
					System.out.print("Please enter the cash amount tendered: ");
					input = scanner.nextLine();
					paid += Double.parseDouble(input);
					cash += paid;

					needToPay -= paid;
					// paid enough
					if (needToPay <= 0) {
						enough = true;
					} else {
						System.out.println("Ooops, not enough.");
					}
				}
				balance += cash;
				change = cash - totalCost;
				System.out.println("Amount of change required: " + change);

				balance -= change;
				System.out.print("Do you want to print a receipt? (enter y to get receipt) ");
				choice = scanner.nextLine();
				while(!choice.equalsIgnoreCase("y") && !choice.equalsIgnoreCase("n")) {
					System.out.println("You have typed wrong, Please reEnter."); 
					System.out.print("Do you want to print a receipt? (enter y to get receipt) ");
					choice = scanner.nextLine();
				}
				if (choice.equalsIgnoreCase("y")){
					System.out.println("***********Receipt***********");
					int num = 1;
					//show all the item in the basket
					for (Transaction t: transcations){
						System.out.println( num + ". Name: *" + t.getName() + "*  Price: *" + t.getCost() + "*"
						+ "    Date: *" + dtf.format(t.getDate())+ "*");
						num ++;
					}
					//payment info
					System.out.println("\nTotal Cost: " + totalCost);
					System.out.println("You have paid: " + cash);
					System.out.println("Change: " + change);
					System.out.println("******Have a good day!******");
					transcations.clear();
				}
				else{
					System.out.println("******Have a good day!******");
				}
			}
			// quit the program
			else if (choice.toLowerCase().equals("n")){
				quit = true;
			}
		}
		System.out.println("\nBalance of the Cash Register: " + balance);
	}
}
